import React, {Component} from 'react'
import './HargaBuah.css'


class HargaBuah extends React.Component {
    render() {
        let dataHargaBuah = [
                    {nama: "Semangka", harga: 10000, berat: 1000},
                    {nama: "Anggur", harga: 40000, berat: 500},
                    {nama: "Strawberry", harga: 30000, berat: 400},
                    {nama: "Jeruk", harga: 30000, berat: 1000},
                    {nama: "Mangga", harga: 30000, berat: 500}
                ]
        const data = dataHargaBuah.map((x) => 
            <tr>
                <td>{x.nama}</td>
                <td>{x.harga}</td>
                <td>{x.berat/1000} kg</td>
            </tr>
        )
        return(
            <div className="content1">
                <h1>Tabel Harga Buah</h1>
                <table>
                    <tr>
                        <th className="th1">Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                    </tr>
                    {data}
                </table>
            </div>
        )
    }
}

export default HargaBuah;