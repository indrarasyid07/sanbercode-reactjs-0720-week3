import React, {Component} from "react"
import './HargaBuah.css'

class HargaBuah1 extends Component{

  constructor(props){
    super(props)
    this.state = {
        dataHargaBuah : [
            {nama: "Semangka", harga: 10000, berat: 1000},
            {nama: "Anggur", harga: 40000, berat: 500},
            {nama: "Strawberry", harga: 30000, berat: 400},
            {nama: "Jeruk", harga: 30000, berat: 1000},
            {nama: "Mangga", harga: 30000, berat: 500}
        ],
        input : {
            nama: "",
            harga: "",
            berat: ""
        },
        indexOfForm: -1    
    }
  }
 
  handleDelete = (event) => {
    let index = event.target.value
    let newdataHargaBuah = this.state.dataHargaBuah
    let editeddataHargaBuah = newdataHargaBuah[this.state.indexOfForm]
    newdataHargaBuah.splice(index, 1)

    if (editeddataHargaBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newdataHargaBuah.findIndex((el) => el === editeddataHargaBuah)
      this.setState({dataHargaBuah: newdataHargaBuah, indexOfForm: newIndex})
      
    }else{
      
      this.setState({dataHargaBuah: newdataHargaBuah})
    }
    
  }
  
  handleEdit = (event) => {
    let index = event.target.value
    let buah = this.state.dataHargaBuah[index]
    this.setState({
        input: {
            nama: buah.nama,
            harga: buah.harga,
            berat: buah.berat
        },
        indexOfForm: index
    })
  }

  handleChange = (event) => {
    let input = {...this.state.input}
    input[event.target.name] = event.target.value
    this.setState({
        input
    });
  }

  handleSubmit= (event) => {
    event.preventDefault()
    let input = this.state.input
    if (input['nama'].replace(/\s/g,'') !== "" && input['harga'].toString().replace(/\s/g, '') !== "" && input['berat'].toString().replace(/\s/g, '') !== ""){      
      let newdataHargaBuah = this.state.dataHargaBuah
      let index = this.state.indexOfForm
      console.log(index)
      if (index === -1){
        newdataHargaBuah = [...newdataHargaBuah, input]
      }else{
        newdataHargaBuah[index] = input
      }
  
      this.setState({
        dataHargaBuah: newdataHargaBuah,
        input: {
            nama:"",
            harga:"",
            berat:""
        },
        indexOfForm: -1
      })
    }
  }

  render(){
    return(
      <>
        <h1 className="content1">Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th className="th1">Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Change</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.dataHargaBuah.map((val,index)=>{
                  return(                    
                    <tr key={index}>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat/1000} kg</td>
                        <td>
                            <button onClick={this.handleEdit} value={index}>Edit</button>
                        &nbsp;
                            <button onClick={this.handleDelete} value={index}>Delete</button>
                        </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1 className="content1">Submit Data Buah</h1>
        <form  className="content1" onSubmit={this.handleSubmit}>
          <label>Nama Buah : </label>          
          <input type="text" name='nama' value={this.state.input.nama} onChange={this.handleChange}/>
          <br/><br/>
          <label>Harga Buah : </label>          
          <input type="text" name='harga' value={this.state.input.harga} onChange={this.handleChange}/>
          <br/><br/>
          <label>Berat Buah : </label>          
          <input type="text" name='berat' value={this.state.input.berat} onChange={this.handleChange}/>
          <br/><br/>
          <button><a>submit</a></button>
          <br/><br/><br/>
        </form>
      </>
    )
  }
}

export default HargaBuah1
