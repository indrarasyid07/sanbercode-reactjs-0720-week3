import React, {useState, useEffect} from "react"
import axios from "axios"
import './HargaBuah1.css'

const HargaBuah2 = () => {

  const [dataHargaBuah, setDataHargaBuah] =  useState(null)
  const [inputNama, setInputNama]  =  useState("")
  const [inputHarga, setInputHarga]  =  useState("")
  const [inputBerat, setInputBerat]  =  useState("")
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if (dataHargaBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setDataHargaBuah(res.data.map(el=>{ return {id: el.id, nama: el.name, harga: el.price, berat: el.weight}} ))
      })
    }
  }, [dataHargaBuah])

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value)

    let newDataHargaBuah = dataHargaBuah.filter(el => el.id != idBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      console.log(res)
    })
          
    setDataHargaBuah([...newDataHargaBuah])
    
  }

  const handleChange = (event) =>{
    let typeOfInput = event.target.name
    switch (typeOfInput) {
      case 'nama':
        {setInputNama(event.target.value)};
        break;
      case 'harga':
        {setInputHarga(event.target.value)};
        break;
      case 'berat':
        {setInputBerat(event.target.value)};
        break;
      default:
        {break;}
    }
  }

  const handleEdit = (event) =>{
    let index = parseInt(event.target.value)
    let buah = dataHargaBuah.find(x=> x.id === index)
    setInputNama(buah.nama)
    setInputHarga(buah.harga)
    setInputBerat(buah.berat)
    setSelectedId(index)
    setStatusForm("edit")
  }

  const handleSubmit = (event) =>{
    event.preventDefault()

    let name = inputNama
    let weight = inputBerat
    let price = inputHarga

    if (name.replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g, '') !== "" && price.toString().replace(/\s/g, '') !== ""){      
      if (statusForm === "create"){        
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name,price,weight})
        .then(res => {
            setDataHargaBuah([...dataHargaBuah, {id: res.data.id, nama: name, harga: price, berat: weight}])
        })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name,price,weight})
        .then(res => {
            let dataBuah = dataHargaBuah.find(el=> el.id === selectedId)
            dataBuah.nama = name
            dataBuah.harga = price
            dataBuah.berat = weight
            setDataHargaBuah([...dataHargaBuah])
        })
      }
      
      setStatusForm("create")
      setSelectedId(0)
      setInputNama("")
      setInputBerat("")
      setInputHarga("")
    }

  }

    return(
      <>
        <h1 className="content1">Tabel Harga Buah</h1>
        <table>
          <thead>
            <tr>
              <th className="th1">Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Change</th>
            </tr>
          </thead>
          <tbody>
              {
                dataHargaBuah !== null && dataHargaBuah.map((val,index)=>{
                  return(                    
                    <tr key={index}>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat/1000} kg</td>
                        <td>
                            <button onClick={handleEdit} value={val.id}>Edit</button>
                        &nbsp;
                            <button onClick={handleDelete} value={val.id}>Delete</button>
                        </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1 className="content1">Submit Data Buah</h1>
        <form  className="content1" onSubmit={handleSubmit}>
          <label>Nama Buah : </label>          
          <input type="text" name='nama' value={inputNama} onChange={handleChange}/>
          <br/><br/>
          <label>Harga Buah : </label>          
          <input type="text" name='harga' value={inputHarga} onChange={handleChange}/>
          <br/><br/>
          <label>Berat Buah : </label>          
          <input type="text" name='berat' value={inputBerat} onChange={handleChange}/>
          <br/><br/>
          <button><a>submit</a></button>
          <br/><br/><br/>
        </form>
      </>
    )
}

export default HargaBuah2
