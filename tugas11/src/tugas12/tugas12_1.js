import React, {Component} from 'react'

class Jam extends Component{
    constructor(props) {
        super(props);
        this.state = {
            time: '',
            batas:0
        };
    }

    componentDidUpdate(){
        if(this.state.time == 0){
            this.componentWillUnmount();
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.Time(),
          1000
        );
        if (this.props.start !== undefined){
            this.setState({batas: this.props.start})
          }
        this.batas = setInterval(
            () => this.Hilang(),
            1000
          );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    Time(){
        this.date= new Date();
        this.h= this.date.getHours();
        if(this.h<=11){
            this.TimeType = 'AM';
        } else {
            this.TimeType = 'PM';
        }
        if(this.h>12){
            this.h = this.h-12;
        }
        if(this.h==0){
            this.h = 12;
        }
        this.m = this.date.getMinutes();
        if(this.m < 10)
        {
            this.m = '0' + this.m.toString();
        }
        this.s = this.date.getSeconds();
        if(this.s < 10)
        {
            this.s = '0' + this.s.toString();
        }
        this.fullTime = this.h.toString() + ':' + this.m.toString() + ':' + this.s.toString() + ' ' + this.TimeType.toString();
        this.setState({
 
            time: this.fullTime
       
        });
    }

    Hilang() {
        this.setState({
            batas: this.state.batas + 1
        });
    }

    render() {
        return (
          <>
            {this.state.batas < 100 &&
            <a>sekarang jam : {this.state.time}</a>}
          </>
        );
    }
}

export default Jam