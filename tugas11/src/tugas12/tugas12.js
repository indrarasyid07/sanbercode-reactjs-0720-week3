import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100,
      time1: ""
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
    this.timerID1 = setInterval(
      () => this.Time(),
      1000
    );
  }

  componentDidUpdate(){
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
    clearInterval(this.timerID1);
  }

  tick() {
    this.setState({
      time: this.state.time - 1, 
    });
  }

  Time(){
    this.date= new Date();
    this.h= this.date.getHours();
    if(this.h<=11){
        this.TimeType = 'AM';
    } else {
        this.TimeType = 'PM';
    }
    if(this.h>12){
        this.h = this.h-12;
    }
    if(this.h==0){
        this.h = 12;
    }
    this.m = this.date.getMinutes();
    if(this.m < 10)
    {
        this.m = '0' + this.m.toString();
    }
    this.s = this.date.getSeconds();
    if(this.s < 10)
    {
        this.s = '0' + this.s.toString();
    }
    this.fullTime = this.h.toString() + ':' + this.m.toString() + ':' + this.s.toString() + ' ' + this.TimeType.toString();
    this.setState({

        time1: this.fullTime
   
    });
}

  render(){
    return(
      <>
        {
          this.state.time > 0 && (
            <>
            <div style={{padding: "100px"}}>
              <h1 style={{float: "left"}}>
                Sekarang jam - {this.state.time1}.
              </h1>
              <h1 style={{float: "right"}}>
                hitung Mundur: {this.state.time}
              </h1>
            </div>
            </>
          )
        }
      </>
    )
  }
}

export default Timer