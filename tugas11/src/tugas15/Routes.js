import React from "react";
import { Switch ,Link,Route } from "react-router-dom";
import './Routes.css';

import HargaBuah from '../tugas11/HargaBuah';
import Timer from '../tugas12/tugas12'
import HargaBuah1 from '../tugas13/HargaBuah'
import HargaBuah2 from '../tugas14/HargaBuah'
import DataBuah from '../tugas15/DataBuah'

const Routes = () => {

  return (
    <>
    <nav>
        <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/timer">Timer</Link>
            </li>
            <li>
              <Link to="/lists">ListClas</Link>
            </li>
            <li>
              <Link to="/hook-lists">ListHooks</Link>
            </li>
            <li>
              <Link to="/context-lists">ListContext</Link>
            </li>
            <li style={{float: "right"}}>
              <a>ColorTheme</a>
              {/*NyerahMas:"*/}
            </li>
        </ul>
    </nav>
    <Switch>
      <Route path="/context-lists">
        <DataBuah />
      </Route> 
      <Route path="/hook-lists">
        <HargaBuah2 />
      </Route> 
      <Route path="/lists">
        <HargaBuah1 />
      </Route> 
      <Route path="/timer">
        <Timer />
      </Route>  
      <Route path="/">
        <HargaBuah/>
      </Route>
    </Switch>
    </>
  );
};

export default Routes;