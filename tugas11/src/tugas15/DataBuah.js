import React from "react"
import {DataBuahProvider} from "./DataBuahContext"
import DataBuahForm from "./DataBuahForm"
import DataBuahList from "./DataBuahList"

const DataBuah = () =>{
  return(
    <DataBuahProvider>
      <DataBuahList/>
      <DataBuahForm/>
    </DataBuahProvider>
  )
}

export default DataBuah